## 关于 AST

- [在线代码转 AST](https://esprima.org/index.html)

- [AST 实战](https://segmentfault.com/a/1190000016231512)

- [AST Query](https://github.com/SBoudrias/AST-query)

- [recast](https://www.npmjs.com/package/recast)

- [Babel 使用全景图](https://m.zhipin.com/mpa/html/get/column?contentId=87995370b56b0ac0qxB70tu1&identity=undefined&userId=undefined)

- [Babel 官网](https://babeljs.io/docs/en/)

## 关于 Cli

> 不得不提到的大佬 TJ：[TJ Holowaychuk](https://github.com/tj)

- 完整的 node.js 命令行解决方案 [Commander](https://github.com/tj/commander.js/blob/master/Readme_zh-CN.md)

![Commander](https://cdn.jsdelivr.net/gh/Matthrews/zm_cdn/images/cammader.png)

- 交互式命令行解决方案 [Inquirer](https://www.npmjs.com/package/inquirer)

![Inquirer](https://cdn.jsdelivr.net/gh/Matthrews/zm_cdn/images/inquirer.png)

- [让你的命令行输出高逼格](https://www.npmjs.com/package/chalk)

![chalk](https://cdn.jsdelivr.net/gh/Matthrews/zm_cdn/images/chalk.png)
