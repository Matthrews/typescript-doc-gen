# typescript-doc-gen

transform TSX interface to Markdown

## 功能

- 识别 interface
- 识别 extends，将其内容转化为子表格
- 识别自定义类型，将其转化为子表并内链

## TODO

## 同类产品

[api-hose](https://github.com/HerbertHe/api-hose)
[react-docgen-typescript](https://www.npmjs.com/package/react-docgen-typescript)
[Documentation](http://documentation.js.org/)
